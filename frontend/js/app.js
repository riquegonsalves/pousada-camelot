// MENU ABRIR MOBILE
if($('.slider .navigation').length){
	var bg;
	$('.slider .navigation .option').click(function(){
		$('.slider .navigation .option').removeClass('active');
		$(this).addClass('active');
		bg =  $(this).attr('data-bg');
		$('.banner').css('background-image', 'url("../'+bg+'")');
	});
}

$('#menuLinks.pictures ul li').click(function(){
	alert('toma');
	// var li = $(this).parent('li');
	// var img = li.attr('data-img');
	//
	// alert(img);
	//
	// event.preventDefault();
});

$('.l-fale').click(function(){
	$('.faleConosco').css('left', 0);
});
$('.l-trabalhe').click(function(){
	$('.trabalheConosco').css('left', 0);
});

$('.modal .close-pi').click(function(){
	$('.modal').css('left', '-100vw');
});

$(document).mouseup(function (e)
{
    var container = $(".modal .container");

    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
    {


				$('.modal').css('left', '-100vw');
    }
});

$('.mobile-head .menu-link, .mobile-close').click(function(){
	$('header').toggleClass('aberto');
});

var mq = window.matchMedia( "(min-width: 860px)" );

if(mq.matches){
	$(document).scroll(function(e){
		var scroll = $(document).scrollTop();

		if(scroll > 100){
			$('.fixheader').css('top', '0px');
		} else {
			$('.fixheader').css('top', '-100vh');
		}
	});
}else{
	$(document).scroll(function(e){
		var scroll = $(document).scrollTop();

		if(scroll > 400){
			$('.mobile-reserva button').slideDown(300);
		} else {
			$('.mobile-reserva button').slideUp(300);
		}
	});
}

$('.mobile-reserva button').click(function(){
	$(this).toggleClass('dois');
	$('#reserva.mobileFixed').slideToggle();
});



$(function() {
    $("#checkinHeader, #checkoutHeader, #checkinBanner, #checkoutBanner, #checkinFooter, #checkoutFooter").datepicker({
        dateFormat: 'DD, d M, yy',
        dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'],
        dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
        dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
        monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
        monthNamesShort: ['JAN','FEV','MAR','ABR','MAI','JUN','JUL','AGO','SET','OUT','NOV','DEZ']
    });

		$("input[data-day='entrada']").datepicker('setDate', new Date());
		var tomorrow = new Date();
		tomorrow.setDate(tomorrow.getDate() + 1);

		$("input[data-day='saida']").datepicker('setDate', tomorrow);
});

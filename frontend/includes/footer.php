<section id="reserva">
	<div class="container">
		<div class="check in">
			<label for="checkin">Entrada</label>
				<input type="text" data-day="entrada" data-type="calendario" name="checkin" id="checkinFooter" value="">
		</div>
		<div class="check out">
			<label for="checkout">Saída</label>
				<input type="text" data-day="saida" data-type="calendario" name="checkout" id="checkoutFooter" value="">
		</div>
		<div class="reserva">
			<button type="submit">Fazer Reserva</button>
		</div>
		<div class="fones">
			<div class="fone">
				(61) 9 9999-9999
			</div>
			<div class="fone">
				(61) 3999-9999
			</div>

		</div>
	</div>
</section>

<footer>
	<div class="container">
		<div class="seguro">
			<img src="img/seguranca.png" alt="" />
		</div>
		<div class="pagamentos">
			<p>
				Métodos de pagamento:
			</p>
			<img src="img/metodos.png" alt="" />
		</div>
	</div>
</footer>

<section class="footer-subs">
	<div class="container">
		<h1>Receba Novidades e Promoções por email!</h1>
		<form action="#">
			<input type="email" name="email" placeholder="e-mail">
			<button type="submit">Cadastrar E-mail</button>
		</form>

		<div class="divisao">

		</div>

		<ul>
			<li>contato@pousadacamelot.com.br</li>
			<li>(62) 3333-9999 | 3333-9999</li>
		</ul>

		<div class="divisao">

		</div>
		<div class="center">
			<ul class="social">
				<li><a href="#" class="facebook"></a></li>
				<li><a href="#" class="instagram"></a></li>
			</ul>
		</div>
	</div>
</section>

<div class="modal faleConosco">
	<div class="container">
		<div class="close-pi">
			x
		</div>

		<div class="text">

			<h2>Fale Conosco</h2>
			<div class="divisao">

			</div>
			<p>
				Podemos ajudar de alguma forma? Entre em contato por telefone, por email ou pelo formulário de contato abaixo. Retornamos dentro de 24h.
			</p>
			<ul>
				<li>• contato@pousadacamelot.com.br</li>
				<li>• (62)3333-9999 | 3333-9999</li>
			</ul>

		</div>

		<div class="form">
			<form class="" action="index.html" method="post">
				<div class="fields">
					<input type="text" name="name" placeholder="nome*">
					<input type="text" name="phone" placeholder="telefone*">
					<input type="email" name="email" placeholder="email">
				</div>
				<div class="textarea">
					<textarea name="message" placeholder="mensagem*"></textarea>
					<button type="submit">ENVIAR</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal trabalheConosco">
	<div class="container">
		<div class="close-pi">
			x
		</div>

		<div class="text">

			<h2>Trabalhe Conosco</h2>
			<div class="divisao">

			</div>
			<p>Quer fazer parte da nossa equipe, aproveitar as oportunidades e crescer junto com a gente? Estamos sempre em busca de pessoas motivadas e dispostas a colocar a mão na massa! Afinal, é com bastante empenho e disposição que a gente consegue oferecer um ótimo serviço para os nossos hóspedes e clientes!</p>
			<p>Informe os seus dados abaixo, anexe seu currículo e entraremos em contato!</p>

		</div>

		<div class="form">
			<form class="" action="index.html" method="post">
				<div class="fields">
					<input type="text" name="name" placeholder="nome*">
					<input type="text" name="phone" placeholder="telefone*">
					<input type="email" name="email" placeholder="email">
				</div>
				<div class="textarea">
					<textarea name="message" placeholder="mensagem*"></textarea>
					<button type="submit">ENVIAR</button>
				</div>
			</form>
		</div>
	</div>
</div>

<script src="js/jquery.js"></script>
<!-- <script src="js/mansory.js"></script> -->
<script type="text/javascript">


function initMap() {

	var camelot = {lat: -14.130552, lng: -47.522969};

	var map;
	var iconBaseBlue = window.location.origin+"/camelot/frontend/img/mapa_pin.png?=123123";

	var styleArray = [{"stylers":[{"hue":"#ff1a00"},{"invert_lightness":true},{"saturation":-100},{"lightness":33},{"gamma":0.5}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#2D333C"}]}];

  map = new google.maps.Map(document.getElementById('map'), {
    center: camelot,
    zoom: 16,
		styles: styleArray,
		draggable: false,
		scrollwheel: false

  });
	// Create a marker and set its position.
	var marker = new google.maps.Marker({
		map: map,
		position: camelot,
		icon: iconBaseBlue
	});


	function moveToLocation(lat, lng){
		var center = new google.maps.LatLng(lat, lng);
		// using global variable:
		map.panTo(center);
		map.setZoom(14);
	}

	$('section#unidades ul li').click(function(){

		eval('var place = ' + $(this).attr('data-id') + ';');
		moveToLocation(place.lat, place.lng);
		$('section#unidades p').html($(this).attr('data-endereco'));

	});
}



    </script>
<script async defer
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDxvkEzFl7J0DNlvnyVmMW23Byx07KmN9I&callback=initMap">
</script>
<script src="js/app.js"></script>
<script src="https://www.jscache.com/wejs?wtype=certificateOfExcellence&amp;uniq=441&amp;locationId=2348776&amp;lang=pt&amp;year=2016&amp;display_version=2"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

</body>
</html>

<div class="preheader">
  <div class="container">
    <ul>
      <li>
        <div class="funcionamento">
          <div class="text">
            <div class="icon-funcionamento"></div>
            <strong><div class="iconCalendar"></div>Horário de Atendimento:</strong>
            <span>Segunda a Domingo: 8h às 23h</span>
          </div>
        </div>
      </li>
      <li>
        <div class="phone">
          <div class="text">
            <div class="icon-phone"></div>
            <span><div class="iconPhone"></div> (62) 3333-9999 | 9999-3333</span>
          </div>
        </div>
      </li>
      <li>
        <div class="links">
          <ul>
            <li>
              <a href="#" class="l-trabalhe">Trabalhe conosco</a>
            </li>
            <li>
              <a href="#" class="l-fale">Fale Conosco</a>
            </li>
          </ul>
        </div>
      </li>
    </ul>
  </div>
</div>

<header>
  <div class="container">
    <a href="#" class="hLogo">
      <img src="img/logo.png" alt="camelot" />
    </a>
    <nav>
      <ul>
        <li><a href="#">Home</a></li>
        <li><a href="#">A Pousada</a></li>
        <li><a href="#">Acomodações</a></li>
        <li><a href="#">Pacotes</a></li>
        <li><a href="#">Tarifário</a></li>
        <li><a href="#">Spa</a></li>
        <li><a href="#">Eventos</a></li>
        <li><a href="#">Passeios</a></li>
      </ul>
    </nav>
    <div class="mobile-close">
    </div>
  </div>
</header>

<div class="fixheader">
  <header>
    <div class="container">
      <a href="#" class="hLogo">
        <img src="img/logo.png" alt="camelot" />
      </a>
      <nav>
        <ul>
          <li><a href="#">Home</a></li>
          <li><a href="#">A Pousada</a></li>
          <li><a href="#">Acomodações</a></li>
          <li><a href="#">Pacotes</a></li>
          <li><a href="#">Tarifário</a></li>
          <li><a href="#">Spa</a></li>
          <li><a href="#">Eventos</a></li>
          <li><a href="#">Passeios</a></li>
        </ul>
      </nav>
      <div class="mobile-close">
        x
      </div>
    </div>
  </header>
  <section id="reserva">
  	<div class="container">
  		<div class="check in">
  			<label for="checkin">Entrada</label>
  			<input type="text" data-day="entrada" data-type="calendario" name="checkin" id="checkinHeader" value="">
  		</div>
  		<div class="check out">
  			<label for="checkout">Saída</label>
  			<input type="text" data-day="saida" data-type="calendario" name="checkout" id="checkoutHeader" value="">
  		</div>
  		<div class="reserva">
  			<button type="submit">Fazer Reserva</button>
  		</div>
  		<div class="fones">
  			<div class="fone">
  				(61) 9 9999-9999
  			</div>
  			<div class="fone">
  				(61) 3999-9999
  			</div>

  		</div>
  	</div>
  </section>
</div>

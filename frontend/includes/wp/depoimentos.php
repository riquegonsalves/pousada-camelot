<li>
  <p>
    “Superou minhas expectativas!! Piscina maravilhosa, pousada muito perto das cachoeiras.”
  </p>
  <div class="autor">
    Sandro
  </div>
  <div class="cidade">
    Brasília, DF
  </div>
</li>
<li>
  <p>
    “O terreno em que a Camelot se localiza tem muito verde e a vista é linda, ideal para contato com a natureza.”
  </p>
  <div class="autor">
    Luiza M.
  </div>
  <div class="cidade">
    Brasília, DF
  </div>
</li>
<li>
  <p>
    “Funcionários extremamente gentis e café bem completo. Voltaremos com certeza.”
  </p>
  <div class="autor">
    Marcos V.
  </div>
  <div class="cidade">
    Brasília, DF
  </div>
</li>

<aside class="sidebar">
  <div class="search">
    <form method="post">
      <input type="text" name="s" placeholder="Busca...">
      <button type="submit"><div class="search-icon"></div></button>
    </form>
  </div>

  <div class="categories">
    <h3>categorias</h3>
    <ul>
      <li>
        <a href="#"> Inteligência Emocional </a>
      </li>
      <li>
        <a href="#"> Inteligência Emocional </a>
      </li>
      <li>
        <a href="#"> Inteligência Emocional </a>
      </li>
      <li>
        <a href="#"> Inteligência Emocional </a>
      </li>
      <li>
        <a href="#"> Inteligência Emocional </a>
      </li>
      <li>
        <a href="#"> Inteligência Emocional </a>
      </li>
    </ul>
  </div>

  <div class="incompany">
    <h3>Capacitação em horatória dentro da sua empresa!</h3>
    <p>
      Já pensou em empoderar os seus funcionários?
    </p>
    <button type="button">Veja o curso incompany</button>
  </div>

  <div class="newsletter">
    <h1>Receba conteúdo por e-mail</h1>
    <p>
      Se inscreva na newsletter para receber nossos conteúdos!
    </p>
    <form class=""  method="post">
      <input type="email" name="email">
      <button type="submit">Inscrever</button>
    </form>
  </div>

</aside>

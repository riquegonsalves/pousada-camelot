<html>
	<head>
		<meta charset="utf-8">
		<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<title>Pousada Camelot</title>
		<link href="stylesheets/screen.css" rel="stylesheet" />
		<script type="text/javascript" src="js/cssrefresh.js"></script>
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">



	</head>
	<body>
		<div class="wrapper">
			<div class="mobile-head">
				<div class="container">
					<div class="menu-link">

					</div>
					<img src="img/logo.png" alt="" class="mobile-logo"/>
				</div>
			</div>
			<div class="mobile-reserva">
				<button>FAZER RESERVA</button>
			</div>
			<section class="mobileFixed" id="reserva">
		  	<div class="container">
		  		<div class="check in">
		  			<label for="checkin">Entrada</label>
		  			<input type="text" data-day="entrada" data-type="calendario" name="checkin" id="checkinHeader" value="">
		  		</div>
		  		<div class="check out">
		  			<label for="checkout">Saída</label>
		  			<input type="text" data-day="saida" data-type="calendario" name="checkout" id="checkoutHeader" value="">
		  		</div>
		  		<div class="reserva">
		  			<button type="submit">Fazer Reserva</button>
		  		</div>
		  		<div class="fones">
		  			<div class="fone">
		  				(61) 9 9999-9999
		  			</div>
		  			<div class="fone">
		  				(61) 3999-9999
		  			</div>

		  		</div>
		  	</div>
		  </section>
		</div>

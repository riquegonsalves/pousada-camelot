
	<?php include 'includes/head.php' ?>
	<section class="banner page passeios">

		<?php include 'includes/header.php' ?>

		<div class="slider">
			<div class="container">

				<div class="content content-1">
					<div class="text">
						<h1>"Diversão faz parte!"</h1>
					</div>

				</div>


			</div>
		</div>
	</section>

	<section class="why">
		<div class="container">
			<h1>Sobre os Passeios</h1>
			<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
			</p>
		</div>
	</section>

	<section class="zigzag">
		<div class="container">
			<ul class="passeios">
				<li>
					<div class="foto">
						<div class="title">
							Passeio de Balão
						</div>
						<div class="price">
							R$ <span>200</span>
						</div>
					</div>
					<div class="text">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					</div>
				</li>
				<li>
					<div class="foto">
						<div class="title">
							Passeio de Balão
						</div>
						<div class="price">
							R$ <span>200</span>
						</div>
					</div>
					<div class="text">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					</div>
				</li>
				<li>
					<div class="foto">
						<div class="title">
							Passeio de Balão
						</div>
						<div class="price">
							R$ <span>200</span>
						</div>
					</div>
					<div class="text">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					</div>
				</li>
			</ul>

		</div>
	</section>

	<section class="pack">
		<div class="container">
			<h1>Os Pacotes</h1>
			<ul class="threeColumn">
				<li>
					<div class="foto">
						<div class="title">
							Passeio de Balão
						</div>
						<div class="price">
							R$ <span>200</span>
						</div>
					</div>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
					</p>
					<p>
						O pacote inclui os passeios:
					</p>
					<ul>
						<li>• Passeio de Balão</li>
						<li>• Passeio de Balão</li>
						<li>• Passeio de Balão</li>
					</ul>
				</li>
				<li>
					<div class="foto">
						<div class="title">
							Passeio de Balão
						</div>
						<div class="price">
							R$ <span>200</span>
						</div>
					</div>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
					</p>
					<p>
						O pacote inclui os passeios:
					</p>
					<ul>
						<li>• Passeio de Balão</li>
						<li>• Passeio de Balão</li>
						<li>• Passeio de Balão</li>
					</ul>
				</li>
				<li>
					<div class="foto">
						<div class="title">
							Passeio de Balão
						</div>
						<div class="price">
							R$ <span>200</span>
						</div>
					</div>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
					</p>
					<p>
						O pacote inclui os passeios:
					</p>
					<ul>
						<li>• Passeio de Balão</li>
						<li>• Passeio de Balão</li>
						<li>• Passeio de Balão</li>
					</ul>
				</li>
			</ul>

			<div class="contratar">
				<div class="info">

				</div>
				<div class="text">
					<h4>QUER CONTRATAR ALGUM PACOTE OU PASSEIO?</h4>
					<p>
						Os pacotes e os passeios serão oferecidos no momento da reserva. Então, escolha o seu pacote ou passeio e faça já a sua reserva no formuário abaixo!
					</p>
				</div>
			</div>
		</div>
	</section>





	<?php include 'includes/footer.php' ?>


	<?php include 'includes/head.php' ?>
	<section class="banner page">

		<?php include 'includes/header.php' ?>

		<div class="slider">
			<div class="container">

				<div class="content content-1">
					<div class="text">
						<h1>"Local ideal para se estar"</h1>
					</div>
				</div>

			</div>
		</div>
	</section>

	<section class="why">
		<div class="container">
			<h1>A Pousada</h1>
			<p>
				A Master é nossa principal suíte. Seu diferencial, além do tamanho e da maravilhosa decoração medieval, é um ambiente separado com uma cama de solteiro, dando maior privacidade ao casal. Esta suíte também possui ar condicionado, frigobar, cobre, TV a cabo e banheiro privativo.
			</p>

			<div class="divisao">

			</div>

			<h1>Comodidades</h1>

			<div class="lists">
				<ul>
					<li>POUSADA</li>
					<li><div class="i"><img src="img/icons/ar.png" alt="" /></div> Ar Condicionado</li>
					<li><div class="i"><img src="img/icons/wifi.png" alt="" /></div> Wi-fi complementar</li>
					<li><div class="i"><img src="img/icons/bar.png" alt="" /></div> Frigobar</li>
					<li><div class="i"><img src="img/icons/tv.png" alt="" /></div> TV 41 polegadas</li>
					<li><div class="i"><img src="img/icons/service.png" alt="" /></div> Serviço de quarto 24h</li>
				</ul>
				<ul>
					<li>QUARTO</li>
					<li><div class="i"><img src="img/icons/ar.png" alt="" /></div> Ar Condicionado</li>
					<li><div class="i"><img src="img/icons/ar.png" alt="" /></div> Wi-fi complementar</li>
					<li><div class="i"><img src="img/icons/ar.png" alt="" /></div> Frigobar</li>
					<li><div class="i"><img src="img/icons/ar.png" alt="" /></div> TV 41 polegadas</li>
					<li><div class="i"><img src="img/icons/ar.png" alt="" /></div> Serviço de quarto 24h</li>
				</ul>
				<ul>
					<li>ALIMENTAÇÃO</li>
					<li><div class="i"><img src="img/icons/ar.png" alt="" /></div> Ar Condicionado</li>
					<li><div class="i"><img src="img/icons/ar.png" alt="" /></div> Wi-fi complementar</li>
					<li><div class="i"><img src="img/icons/ar.png" alt="" /></div> Frigobar</li>
					<li><div class="i"><img src="img/icons/ar.png" alt="" /></div> TV 41 polegadas</li>
					<li><div class="i"><img src="img/icons/ar.png" alt="" /></div> Serviço de quarto 24h</li>
				</ul>

			</div>

			<div class="divisao">

			</div>

		</div>
	</section>

	<section class="tour">
		<div class="container">
			<div class="video">
				<iframe style="width: 100%; height: 100%; display: block" src="https://www.youtube.com/embed/q6jCjo55eu4" frameborder="0" allowfullscreen></iframe>
			</div>
			<div class="text">
				<h2>Venha fazer um Tour</h2>
				<p>
					Temos um tour virtual que te permite visitar todos os cômodos e áreas comuns e observá-los com visão 360º, ou seja, é possível ver o chão, o teto, as paredes e a decoração de cada parte da pousada!
				</p>

				<a href="#">Passeio Virtual >></a>
			</div>

		</div>
	</section>

	<section id="menuLinks">
		<div class="container">
			<ul>
				<li>
					<a href="#">
						<span class="title">Acomodações</span>
					</a>
				</li>
				<li>
					<a href="#">
						<span class="title">Acomodações</span>
					</a>
				</li>
				<li>
					<a href="#">
						<span class="title">Acomodações</span>
					</a>
				</li>
				<li>
					<a href="#">
						<span class="title">Acomodações</span>
					</a>
				</li>
				<li>
					<a href="#">
						<span class="title">Acomodações</span>
					</a>
				</li>
				<li>
					<a href="#">
						<span class="title">Acomodações</span>
					</a>
				</li>
				<li>
					<a href="#">
						<span class="title">Acomodações</span>
					</a>
				</li>
				<li>
					<a href="#">
						<span class="title">Acomodações</span>
					</a>
				</li>
			</ul>
		</div>
	</section>


	<div id="map">

	</div>

	<?php include 'includes/footer.php' ?>


	<?php include 'includes/head.php' ?>
	<?php include 'includes/header.php' ?>


	<section class="tarifario">
		<div class="container">
			<h1>Tarifário</h1>

			<div class="left">
				<div class="title">
					Baixa Temporada
				</div>
				<div class="subtitle">
					(Março a Junho - Agosto a Novembro)
				</div>

				<table>
					<thead>
						<tr>
							<td>
								Nº Pessoas
							</td>
							<td>
								Suíte Luxo
							</td>
							<td>
								Luxo especial
							</td>
							<td>
								Master
							</td>
						</tr>
					</thead>
					<tr>
						<td>
							2 pessoas
						</td>
						<td>
							R$ 300,00
						</td>
						<td>
							R$ 300,00
						</td>
						<td>
							R$ 300,00
						</td>
					</tr>
					<tr>
						<td>
							3 pessoas
						</td>
						<td>
							R$ 300,00
						</td>
						<td>
							R$ 300,00
						</td>
						<td>
							R$ 300,00
						</td>
					</tr>
					<tr>
						<td>
							4 pessoas
						</td>
						<td>
							R$ 300,00
						</td>
						<td>
							R$ 300,00
						</td>
						<td>
							R$ 300,00
						</td>
					</tr>
				</table>

			</div>

			<div class="right">
				<div class="title">
					Alta Temporada
				</div>
				<div class="subtitle">
					(Dezembro, Janeiro, Fevereiro e Julho)
				</div>

				<table>
					<thead>
						<tr>
							<td>
								Nº Pessoas
							</td>
							<td>
								Suíte Luxo
							</td>
							<td>
								Luxo especial
							</td>
							<td>
								Master
							</td>
						</tr>
					</thead>
					<tr>
						<td>
							2 pessoas
						</td>
						<td>
							R$ 300,00
						</td>
						<td>
							R$ 300,00
						</td>
						<td>
							R$ 300,00
						</td>
					</tr>
					<tr>
						<td>
							3 pessoas
						</td>
						<td>
							R$ 300,00
						</td>
						<td>
							R$ 300,00
						</td>
						<td>
							R$ 300,00
						</td>
					</tr>
					<tr>
						<td>
							4 pessoas
						</td>
						<td>
							R$ 300,00
						</td>
						<td>
							R$ 300,00
						</td>
						<td>
							R$ 300,00
						</td>
					</tr>
				</table>

			</div>
		</div>
		<div class="container">
			<div class="divisao"></div>
			<p>
				* para valores de feriados e datas comemorativas, consulte os <a href="#">nossos pacotes</a>
			</p>
		</div>
	</section>

	<?php include 'includes/footer.php' ?>

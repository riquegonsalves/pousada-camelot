
	<?php include 'includes/head.php' ?>
	<section class="banner">

		<?php include 'includes/header.php' ?>

		<div class="slider">
			<div class="container">

				<div class="content content-1">
					<div class="text">
						<h1>PACOTE CORPUS CHRIST</h1>
					</div>

					<div class="reserva">
						<h1>FAÇA SUA <span>RESERVA ONLINE</span></h1>
						<div class="check check-in">
							<span>Entrada:</span>
							<input type="text" data-day="entrada" data-type="calendario" name="checkin" id="checkinBanner" value="">
						</div>
						<div class="check check-out">
							<span>Saída:</span>
							<input type="text" data-day="saida" data-type="calendario" name="checkout" id="checkoutBanner" value="">
						</div>

						<button type="button" name="button">Fazer reserva</button>

						<span class="ou">OU LIGUE</span>
						<div class="tel">
							(61) 3333-9999
						</div>
						<div class="tel">
							(61) 3333-9999
						</div>
					</div>

				</div>

				<div class="navigation">
					<div data-bg="img/banner/home1.jpg" class="option option-1 active"></div>
					<div data-bg="img/banner/passeios.jpg" class="option option-2"></div>
					<div data-bg="img/banner/acomodacoes.jpg" class="option option-3"></div>
				</div>

			</div>
		</div>
	</section>

	<section class="why">
		<div class="container">
			<h1>Por que você vai gostar?</h1>
			<p>
				A Master é nossa principal suíte. Seu diferencial, além do tamanho e da maravilhosa decoração medieval, é um ambiente separado com uma cama de solteiro, dando maior privacidade ao casal. Esta suíte também possui ar condicionado, frigobar, cobre, TV a cabo e banheiro privativo.
			</p>
		</div>
	</section>

	<section class="infos">
		<div class="container">

			<h1>Comodidades</h1>
			<div class="lists">
				<ul>
					<li>POUSADA</li>
					<li><div class="i"><img src="img/icons/ar.png" alt="" /></div> Ar Condicionado</li>
					<li><div class="i"><img src="img/icons/wifi.png" alt="" /></div> Wi-fi complementar</li>
					<li><div class="i"><img src="img/icons/bar.png" alt="" /></div> Frigobar</li>
					<li><div class="i"><img src="img/icons/tv.png" alt="" /></div> TV 41 polegadas</li>
					<li><div class="i"><img src="img/icons/service.png" alt="" /></div> Serviço de quarto 24h</li>
				</ul>
				<ul>
					<li>QUARTO</li>
					<li><div class="i"><img src="img/icons/ar.png" alt="" /></div> Ar Condicionado</li>
					<li><div class="i"><img src="img/icons/ar.png" alt="" /></div> Wi-fi complementar</li>
					<li><div class="i"><img src="img/icons/ar.png" alt="" /></div> Frigobar</li>
					<li><div class="i"><img src="img/icons/ar.png" alt="" /></div> TV 41 polegadas</li>
					<li><div class="i"><img src="img/icons/ar.png" alt="" /></div> Serviço de quarto 24h</li>
				</ul>
				<ul>
					<li>ALIMENTAÇÃO</li>
					<li><div class="i"><img src="img/icons/ar.png" alt="" /></div> Ar Condicionado</li>
					<li><div class="i"><img src="img/icons/ar.png" alt="" /></div> Wi-fi complementar</li>
					<li><div class="i"><img src="img/icons/ar.png" alt="" /></div> Frigobar</li>
					<li><div class="i"><img src="img/icons/ar.png" alt="" /></div> TV 41 polegadas</li>
					<li><div class="i"><img src="img/icons/ar.png" alt="" /></div> Serviço de quarto 24h</li>
				</ul>

			</div>

			<div class="line">

			</div>

			<h1>Diárias</h1>

			<ul class="prices">
				<li>
					<div class="left">
						<div class="persons two"></div>
						<div class="text">
							(até duas pessoas)
						</div>
					</div>
					<div class="value">a partir de <span>R$ 300,00</span></div>
				</li>
				<li>
					<div class="left">
						<div class="persons three"></div>
						<div class="text">
							(três pessoas)
						</div>
					</div>
					<div class="value">a partir de <span>R$ 300,00</span></div>
				</li>
				<li>
					<div class="left">
						<div class="persons four"></div>
						<div class="text">
							(quatro pessoas)
						</div>
					</div>
					<div class="value">a partir de <span>R$ 300,00</span></div>
				</li>
			</ul>
			<div class="obs">
				* para consultar os valores de baixa/alta temporada, feriados e datas comemorativas, <a href="#">veja nosso tarifário.</a>
			</div>

		</div>
	</section>

	<section class="more">
		<div class="container">
			<h1>Mais informações</h1>

			<ul>
				<li>• Check-in tal hora e check-out tal hora</li>
				<li>• Todos os quartos garantem a acomodação de, pelo menos, 2 pessoas.</li>
				<li>• É proibido fumar em qualquer quarto da pousada</li>
				<li>• São necessários documento com foto e cartão de crédito no momento do check-in</li>
			</ul>

		</div>
	</section>

	<?php include 'includes/footer.php' ?>

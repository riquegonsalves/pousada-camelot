
	<?php include 'includes/head.php' ?>
	<section class="banner">

		<?php include 'includes/header.php' ?>

		<div class="slider">
			<div class="container">

				<div class="content content-1">
					<div class="text">
						<h1>NO CORAÇÃO DA CHAPADA DOS VEADEIROS</h1>
						<h2>APROVEITE A CHAPADA COM TRANQUILIDADE, LAZER E CONFORTO</h2>
					</div>

					<div class="reserva">
						<h1>FAÇA SUA <span>RESERVA ONLINE</span></h1>
						<div class="check check-in">
							<span>Entrada:</span>
							<input type="text" data-day="entrada" data-type="calendario" name="checkin" id="checkinBanner" value="">
						</div>
						<div class="check check-out">
							<span>Saída:</span>
							<input type="text" data-day="saida" data-type="calendario" name="checkout" id="checkoutBanner" value="">
						</div>

						<button type="button" name="button">Fazer reserva</button>

						<span class="ou">OU LIGUE</span>
						<div class="tel">
							(61) 3333-9999
						</div>
						<div class="tel">
							(61) 3333-9999
						</div>
					</div>

				</div>


			</div>
		</div>
	</section>


	<section id="confianca">
		<h1>FAÇA SUA RESERVA COM CONFIANÇA</h1>
		<ul>
			<li>
				<img src="img/check.png" alt="" />
				<h3>Reservas sem taxas</h3>
				<p>
					Fazendo a sua reserva por aqui, você garante o melhor preço em comparação com os sites de reserva ( booking.com, decolar.com, etc )
				</p>
			</li>
			<li>
				<img src="img/check.png" alt="" />
				<h3>Pagamento Seguro</h3>
				<p>Ao fazer o pagamento aqui pelo site, nós garantimos a segurança e privacidade total dos seus dados</p>
			</li>
			<li>
				<img src="img/check.png" alt="" />
				<h3>Atendimento Fácil</h3>
				<p>Teve algum problema ou dúvida? Estamos à sua disposição todos os dias, das 8h às 23h, por telefone, email ou pelo chat online.</p>
			</li>
		</ul>
	</section>

	<?php include 'includes/wp/menulinks.php' ?>



	<section id="testimonials">
		<div class="container">
			<h1>Reconhecidos pela execelência</h1>
			<ul>
			  <li>

					<a target="_blank" href="https://www.tripadvisor.com.br/Hotel_Review-g2159104-d2348776-Reviews-Pousada_Camelot_Inn-Alto_Paraiso_de_Goias_Chapada_dos_Veadeiros_National_Park_State_o.html">
						<img src="img/taCert.jpg" alt="TripAdvisor" class="widCOEImg" id="CDSWIDCOELOGO"/>
					</a>

			  </li>
			  <?php  include 'includes/wp/depoimentos.php' ?>
			</ul>
			<div class="buttons">
				<a target="_blank" href="https://www.tripadvisor.com.br/UserReview-g2159104-d2348776-ehttp%3A__2F____2F__www__2E__tripadvisor__2E__com__2E__br__2F__Hotel__5F__Review__2D__g2159104__2D__d2348776__2D__Reviews__2D__Pousada__5F__Camelot__5F__Inn__2D__Alto__5F__Paraiso__5F__de__5F__Goias__5F__Chapada__5F__dos__5F__Veadeiros__5F__National__5F__Park__5F__State__5F__o__2E__html-Pousada_Camelot_Inn-Alto_Paraiso_de_Goias_Chapada_dos_Veadeiros_National_Park_State_of_Goias.html">
					<button>Avaliar</button>
				</a>
				<a target="_blank" href="https://www.tripadvisor.com.br/Hotel_Review-g2159104-d2348776-Reviews-Pousada_Camelot_Inn-Alto_Paraiso_de_Goias_Chapada_dos_Veadeiros_National_Park_State_o.html">
					<button>Ver Avaliações</button>
				</a>
			</div>

		</div>
	</section>

	<section id="map">

	</section>
	<section id="socials">
		<div class="container">
			<div class="face">
				<h2>Facebook</h2>
				<div class="bg">
					<p class="numFace">
						29.000
					</p>
					<p class="people">
						PESSOAS QUE CURTEM EXPERIÊNCIAS MÁGICAS
					</p>
				</div>
				<a class="button" href="http://facebook.com/pousadacamelot" target="_blank">Curtir no Facebook</a>
			</div>

			<div class="instagram">
				<h2>Instagram</h2>
				<div class="pics">
					<script src="https://snapwidget.com/js/snapwidget.js"></script>
					<iframe src="https://snapwidget.com/embed/203916" class="snapwidget-widget" allowTransparency="true" frameborder="0" scrolling="no" style="border:none; overflow:hidden; width:100%; "></iframe>
					<div class="number">
						<span>1.000</span> PESSOAS QUE SEGUEM EXPERIÊNCIAS MÁGICAS
					</div>

					<a class="button" href="http://instagram.com/pousadacamelot">Seguir no Instagram</a>
				</div>
			</div>
		</div>
	</section>



	<?php include 'includes/footer.php' ?>

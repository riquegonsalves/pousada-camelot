<?php include 'includes/head.php' ?>
<?php include 'includes/header.php' ?>
<div class="breadcrumb">
  <div class="container">
      <ul>
        <li>
          <a href="#">Home</a>
        </li>
        <li>
          <a href="#">Blog</a>
        </li>
        </li>
        <li>
          <a href="#">Post</a>
        </li>
      </ul>
  </div>
</div>

<section id="blog">
  <div class="container">

    <div class="main">
      <article>
        <h1>Como se comportar em uma entrevista de emprego?</h1>
        <div class="info">
          <ul>
            <li>
              <div class="hour"></div>
              29 de Abril, 2016
            </li>

            <li>
              Postado por: <a href="#">Admin</a>
            </li>

            <li>
              Categoria: <a href="#">Inteligência Emocional</a>
            </li>

            <li>
              10 Comentários <span class="comment"></span>
            </li>
          </ul>
        </div>

        <div class="pic">
          <img src="img/thumb-post.jpg" alt="" />
          <div class="play">
            p
          </div>
        </div>

        <div class="content">
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </p>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </p>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </p>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </p>
        </div>

      </article>
    </div>
    <?php include 'includes/sidebar.php' ?>
  </div>
</section>
<?php include 'includes/footer.php' ?>

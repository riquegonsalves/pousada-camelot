<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>
	<div class="notfounddiv">
		<div class="container">
			<h1 class="fourcinza">404</h1>
			<h1>404</h1>
			<h2>Ops! A página que você tentou acessar não foi encontrada nos arredores de Camelot!</h2>
			<h2>Estaria você buscando alguma informação sobre <a href="/acomodacoes">as nossas acomodações</a>?</h2>
		</div>
	</div>

<?php get_footer(); ?>
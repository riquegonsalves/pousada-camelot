<?php $args = array(
              'post_type'			=> 'depoimento',
              'posts_per_page'	=> 3
            );

        $queryPosts = query_posts($args); ?>


<?php if(have_posts()): ?>

        <?php
          $contador = 1;
          while (have_posts()) : the_post(); ?>

          <li>
            <p>
              <?php the_content(); ?>
            </p>
            <div class="autor">
              <?php the_title(); ?>
            </div>
            <div class="cidade">
              <?= get_field('cidade') ?>
            </div>
          </li>


        <?php

          endwhile; ?>

<?php endif; ?>

<?php wp_reset_query(); ?>

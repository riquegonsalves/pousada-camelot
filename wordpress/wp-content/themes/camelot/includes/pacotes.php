<?php $args = array(
              'post_type'			=> 'pacote',
              'posts_per_page'	=> -1
            );

        $queryPosts = query_posts($args); ?>


<?php if(have_posts()): ?>

  <ul class="passeios">
    <?php while (have_posts()) : the_post(); ?>
      <li>
        <a href="<?= get_permalink() ?>">

          <?php
          $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
          ?>
          <div class="foto pacote" style="background: url('<?= $feat_image ?>'); background-position: center; background-size: cover">
            <div class="title correcaotitle">
              <?php the_title(); ?>
            </div>
            <?php
              $entrada = get_field('entrada');
              $entrada = new DateTime($entrada);
              $saida = get_field('saida');
              $saida = new DateTime($saida);
             ?>
            <span class="quantidade correcaoquantidade">de <?= $entrada->format('d/m') ?> a <?= $saida->format('d/m') ?></span>
            <div class="preco correcaoprecoplus">
              <span>a partir de</span>
              <?php $price = explode(',', get_field('price_two')) ?>
              <span class="rs">R$ <?= $price[0] ?></span>
            </div>
          </div>
          <div class="text">
            <?php the_content(); ?>
          </div>
        </a>
      </li>
    <?php endwhile; ?>
	</ul>


<?php endif; ?>

<?php wp_reset_query(); ?>

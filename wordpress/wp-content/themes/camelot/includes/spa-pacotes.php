<?php $args = array(
              'post_type'			=> 'pacotespa',
              'posts_per_page'	=> -1
            );

        $queryPosts = query_posts($args); ?>


<?php if(have_posts()): ?>
  <ul class="threeColumn">
    <?php while (have_posts()) : the_post(); ?>

      <li class="clickContratarSpa">
        <?php
        $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
        ?>
        <div class="foto" style="background: url('<?= $feat_image ?>'); background-position: center; background-size: cover">
          <div class="title">
            <?php the_title(); ?>
          </div>
          <div class="price">
            R$ <span><?= get_field('price') ?></span>
          </div>
        </div>
        <p>
          <?php the_content(); ?>
        </p>

        <?php if(get_field('pacote')): ?>
            <p>
              O pacote inclui os passeios:
            </p>
            <ul>
              <?php while(has_sub_field('pacote')): ?>

                    <li>• <?php the_sub_field('item') ?> </li>

                <?php endwhile; ?>
            </ul>
          <?php endif; ?>


      </li>
    <?php endwhile; ?>

<?php endif; ?>

<?php wp_reset_query(); ?>

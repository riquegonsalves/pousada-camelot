<?php
  $postid = url_to_postid( get_permalink() );
?>

<?php if(get_field('slider', $postid)): ?>
  <div class="prevSlider navIcon"><</div>
  <div class="nextSlider navIcon">></div>
  <div class="navigation">
    <?php $contador = 0; while(has_sub_field('slider', $postid)):  ?>
      <div data-bg="<?php the_sub_field('imagem', $postid) ?>" data-order="<?= $contador+1 ?>" class="option option-<?= $contador+1 ?> <?php if($contador == 0): ?>active<?php endif; ?>"></div>
    <?php $contador++; endwhile; ?>
  </div>

<?php endif; ?>

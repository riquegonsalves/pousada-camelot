<?php if(get_field('pousada')): ?>
  <ul>
    <li>POUSADA</li>
    <?php while(has_sub_field('pousada')): ?>
        <li><div class="i"><img src="<?php the_sub_field('icone') ?>" /></div> <?php the_sub_field('item') ?></li>
      <?php endwhile; ?>
  </ul>
<?php endif; ?>
<?php if(get_field('quarto')): ?>
  <ul>
    <li>QUARTO</li>
    <?php while(has_sub_field('quarto')): ?>
        <li><div class="i"><img src="<?php the_sub_field('icone') ?>" /></div> <?php the_sub_field('item') ?></li>
      <?php endwhile; ?>
  </ul>
<?php endif; ?>
<?php if(get_field('alimentacao')): ?>
  <ul>
    <li>ALIMENTAÇÃO</li>
    <?php while(has_sub_field('alimentacao')): ?>
        <li><div class="i"><img src="<?php the_sub_field('icone') ?>" /></div> <?php the_sub_field('item') ?></li>
      <?php endwhile; ?>
  </ul>
<?php endif; ?>

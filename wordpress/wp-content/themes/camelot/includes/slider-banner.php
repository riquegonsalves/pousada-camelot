<?php
  $postid = url_to_postid( get_permalink() );
?>

<?php if(get_field('slider', $postid)): $contador = 0;?>

    <?php while(has_sub_field('slider', $postid)): ?>
      <?php if($contador==0){
          $feat_image =  get_sub_field('imagem', $postid);
      } ?>
    <?php $contador++; endwhile; ?>

    <style>
    section.banner.page{
      background: url('<?= $feat_image ?>') ;
    }

    section.banner.page .slider .container{
      min-height: 460px
    }
    </style>
<?php endif; ?>

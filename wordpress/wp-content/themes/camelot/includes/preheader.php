<?php include 'information.php' ?>
<div class="preheader">
  <div class="container">
    <ul>
      <li>
        <div class="funcionamento">
          <div class="text">
            <div class="icon-funcionamento"></div>
            <strong><div class="iconCalendar"></div>Horário de Atendimento:</strong>
            <span><?= $horario_de_atendimento ?></span>
          </div>
        </div>
      </li>
      <li>
        <div class="phone">
          <div class="text">
            <div class="icon-phone"></div>
            <span><div class="iconPhone"></div><?= $fone1 ?> | <?= $fone2 ?></span>
          </div>
        </div>
      </li>
      <li>
        <div class="links">
          <ul>
            <li>
              <a href="#" class="l-trabalhe">• Trabalhe conosco</a>
            </li>
            <li>
              <a href="#" class="l-fale">• Fale Conosco</a>
            </li>
          </ul>
        </div>
      </li>
    </ul>
  </div>
</div>

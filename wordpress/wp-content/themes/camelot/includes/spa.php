<?php $args = array(
              'post_type'			=> 'terapia',
              'posts_per_page'	=> -1
            );

        $queryPosts = query_posts($args); ?>


<?php if(have_posts()): ?>
  <ul class="passeios">
    <?php while (have_posts()) : the_post(); ?>
      <li class="clickContratarSpa">
        <?php
        $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
        ?>
        <div class="foto" style="background: url('<?= $feat_image ?>')">
          <div class="title">
            <?php the_title(); ?>
          </div>
          <div class="price">
            R$ <span><?= get_field('price') ?></span>
          </div>
        </div>
        <div class="text">
          <?php the_content(); ?>
        </div>
      </li>
    <?php endwhile; ?>
	</ul>
<?php endif; ?>

<?php wp_reset_query(); ?>

<?php $args = array(
              'post_type'			=> 'fotochapada',
              'posts_per_page'	=> -1
            );

        $queryPosts = query_posts($args); ?>


<?php if(have_posts()): ?>
	<section id="menuLinks" class="pictures">
		<div class="container">
			<ul>
        <?php
          while (have_posts()) : the_post(); ?>

					<?php
	                $feat_image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );

					?>
					<li style="background: url('<?= $feat_image[0] ?>'); background-size: cover; background-position: center" data-img="<?= $feat_image[0] ?>">
							<a href="#">
								<span class="title"><?php the_title(); ?></span>
							</a>
					</li>


        <?php

          endwhile; ?>
				</ul>
			</div>
		</section>
<?php endif; ?>

<?php wp_reset_query(); ?>

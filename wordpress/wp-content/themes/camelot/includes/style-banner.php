<?php
  $postid = url_to_postid( get_permalink() );
?>

<?php if(get_field('banner', $postid)): ?>
  <style>
   .banner{
     background: url('<?= get_field('banner', $postid) ?>') !important;
   }
  </style>
<?php endif; ?>

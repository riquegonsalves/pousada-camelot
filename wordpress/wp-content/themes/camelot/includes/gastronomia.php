<?php if(get_field('refeicoes')): ?>
  <ul class="passeios">
    <?php while(has_sub_field('refeicoes')): ?>
      <li>
        <?php
        $feat_image = get_sub_field('foto');
        ?>
        <div class="foto" style="background: url('<?= $feat_image ?>'); background-size: cover; background-position: center">
          <div class="title">
            <?php the_sub_field('nome'); ?>
          </div>
        </div>
        <div class="text">
          <?php the_sub_field('texto'); ?>
        </div>
      </li>
    <?php endwhile; ?>
	</ul>
<?php endif; ?>

<?php
$conteudo = '9';
$ambiente = $_GET['ambiente'];

if ($ambiente == "bar" ){
	
	$tour = "amb1.swf";
	
} elseif ($ambiente == "cafe"){
	
	$tour = "amb2.swf";
	
} elseif ($ambiente == "piscina_noite"){
	
	$tour = "amb3.swf";
	
} elseif ($ambiente == "piscina_dia"){
	
	$tour = "amb4.swf";
	
} elseif ($ambiente == "jardim"){
	
	$tour = "amb5.swf";
	
} elseif ($ambiente == "suit_master"){
	
	$tour = "amb6.swf";
	
} elseif ($ambiente == "suit_luxo"){
	
	$tour = "amb7.swf";
	
} elseif ($ambiente == "suit_luxo2"){
	
	$tour = "amb8.swf";
	
} elseif ($ambiente == "suit_luxo1"){
	
	$tour = "amb9.swf";
	
} elseif ($ambiente == "tavola_redonda"){
	
	$tour = "amb10.swf";
	
} elseif ($ambiente == "entrada"){
	
	$tour = "amb11.swf";
	
} elseif ($ambiente == ""){
	
	$tour = "amb1.swf";
	
}
?>

<?php include_once 'topo.php'; ?>

<div class="conteudo_central">

    <div id="conteudo_esquerda">
    
    	<span class="titulo">Mais Ambientes 360º</span>
        
        <br /><br />
        
        <a href="?ambiente=bar"><div class="apartamentos">• Bar</div></a>
        <a href="?ambiente=cafe"><div class="apartamentos">• Café</div></a>
        <a href="?ambiente=piscina_noite"><div class="apartamentos">• Piscina Noite</div></a>
        <a href="?ambiente=piscina_dia"><div class="apartamentos">• Piscina Dia</div></a>
        <a href="?ambiente=jardim"><div class="apartamentos">• Jardim</div></a>
        <a href="?ambiente=suit_master"><div class="apartamentos">• Suite Máster</div></a>
        <a href="?ambiente=suit_luxo"><div class="apartamentos">• Suite Luxo Especial</div></a>
        <a href="?ambiente=suit_luxo1"><div class="apartamentos">• Suite Luxo 1</div></a>
        <a href="?ambiente=suit_luxo2"><div class="apartamentos">• Suite Luxo 2</div></a>
        <a href="?ambiente=tavola_redonda"><div class="apartamentos">• Távola Redonda</div></a>
        <a href="?ambiente=entrada"><div class="apartamentos">• Entrada</div></a>
            
		<br /><br />
        
		<?php include_once 'modulo_lateral_facebook.php'; ?>
        
    </div>
    
    <div id="conteudo_direita">
    
    	<?php echo Conteudo($conteudo); ?>
        
        <br /><br />
    
    	<iframe src="tour_virtual/<?php echo $tour; ?>" width="750" height="500" frameborder="0" scrolling="no"></iframe>
        
    </div>
    
    <!-- LIMPA -->
    <div class="limpa"></div>
    
</div>
    

<?php include_once 'rodape.php'; ?>
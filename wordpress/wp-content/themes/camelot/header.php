<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<link href="<?php bloginfo('template_url') ?>/img/favicon_camelot.png" rel="icon" type="image/png" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<meta charset="utf-8">
	<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<title>Pousada Camelot</title>
	<link href="<?php bloginfo('template_url') ?>/stylesheets/screen.css" rel="stylesheet" />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

	<!--Start of Tawk.to Script-->
		<script type="text/javascript">
		var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
		(function(){
		var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
		s1.async=true;
		s1.src='https://embed.tawk.to/578534c135e249fc6cfa6c1f/default';
		s1.charset='UTF-8';
		s1.setAttribute('crossorigin','*');
		s0.parentNode.insertBefore(s1,s0);
		})();
		</script>
	<!--End of Tawk.to Script-->

</head>


<body <?php body_class(); ?>>
	<?php include 'includes/information.php' ?>
	<div class="wrapper">
		<div class="mobile-head">
			<div class="container">
				<div class="menu-link">

				</div>
				<img src="<?php bloginfo('template_url') ?>/img/logo.png" alt="" class="mobile-logo"/>
			</div>
		</div>

		<?php if (!is_page( 'checkout' ) ): ?>
			<div class="mobile-reserva">
				<button>FAZER RESERVA<div class="reserva-seta"></div></button>
			</div>
			<section class="mobileFixed" id="reserva">
				<div class="container">
					<form action="<?php bloginfo('url') ?>/checkout" method="get">
						<div class="check in">
							<label for="checkin">Entrada:</label>
							<input readonly class="readonly" type="text" data-day="entrada" data-type="calendario" name="checkin" id="checkinH" value="">
							<input type="hidden" name="date_format" value="d/m/Y">
							<input type="hidden" name="widget_date" value="">
						</div>
						<div class="check out">
							<label for="checkout">Saída:</label>
							<input readonly class="readonly" type="text" data-day="saida" data-type="calendario" name="checkout" id="checkoutH" value="">
							<input type="hidden" name="widget_date_to" value="">
						</div>
						<div class="reserva">
							<button type="submit">Fazer Reserva</button>
						</div>
						<div class="fones">
							<div class="fone">
								<?= $fone1 ?>
							</div>
							<div class="fone">
								<?= $fone2 ?>
							</div>
						</div>
					</form>
				</div>
			</section>
		<?php endif; ?>
	</div>

 <?php include 'includes/preheader.php' ?>

	<header>
	  <div class="container">
	    <a href="<?php bloginfo('url') ?>" class="hLogo">
	      <img src="<?php bloginfo('template_url') ?>/img/logo.png" alt="camelot" />
	    </a>
			<nav class="main">
			 <?php wp_nav_menu (array('theme_location' => 'primary-menu','menu_class' => 'nav'));?>
		 </nav>
		 <nav class="mobile">
			 <?php wp_nav_menu (array('theme_location' => 'secondary-menu','menu_class' => 'nav'));?>
			 <div class="links">
				 <a href="#" class="l-trabalhe">• Trabalhe conosco</a>
				 <a href="#" class="l-fale">• Fale conosco</a>
			 </div>
		 </nav>
	    <div class="mobile-close">
	    </div>
	  </div>
	</header>


	<?php if (!is_page( 'checkout' ) ): ?>
		<div class="fixheader">
		  <header>
		    <div class="container">
		      <a href="<?php bloginfo('url') ?>" class="hLogo">
		        <img src="<?php bloginfo('template_url') ?>/img/logo.png" alt="camelot" />
		      </a>
		      <nav>
		        <?php wp_nav_menu (array('theme_location' => 'primary-menu','menu_class' => 'nav'));?>
		      </nav>
		      <div class="mobile-close">
		        x
		      </div>
		    </div>
		  </header>
		  <section id="reserva">
		  	<div class="container">
					<form action="<?php bloginfo('url') ?>/checkout" method="get">

						<div class="check in">
							<label for="checkin">Entrada:</label>
							<input readonly class="readonly" type="text" data-day="entrada" data-type="calendario" name="checkin" id="checkinHeader" value="">
							<input type="hidden" name="date_format" value="d/m/Y">
							<input type="hidden" name="widget_date" value="">
						</div>
						<div class="check out">
							<label for="checkout">Saída:</label>
							<input readonly class="readonly" type="text" data-day="saida" data-type="calendario" name="checkout" id="checkoutHeader" value="">
							<input type="hidden" name="widget_date_to" value="">
						</div>
						<div class="reserva">
							<button type="submit">Fazer Reserva</button>
						</div>
						<div class="fones">
							<div class="fone">
								<?= $fone1 ?>
							</div>
							<div class="fone">
								<?= $fone2 ?>
							</div>

						</div>
					</form>
		  	</div>
		  </section>
		</div>
	<?php endif; ?>

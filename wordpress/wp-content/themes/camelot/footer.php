<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<?php if (!is_page( 'eventos' ) ): ?>
  <?php include 'includes/information.php' ?>
  <section id="reserva" class="footReserva">
  	<div class="container">
  		<form action="/checkout" method="get">
  			<div class="check in">
  				<label for="checkin">Entrada:</label>
  					<input readonly type="text" data-day="entrada" data-type="calendario" name="checkin" id="checkinFooter" value="">
  					<input type="hidden" name="date_format" value="d/m/Y">
  					<input type="hidden" name="widget_date" value="">
  			</div>
  			<div class="check out">
  				<label for="checkout">Saída:</label>
  					<input readonly type="text" data-day="saida" data-type="calendario" name="checkout" id="checkoutFooter" value="">
  					<input type="hidden" name="widget_date_to" value="">
  			</div>
  			<div class="reserva">
  				<button type="submit">Fazer Reserva</button>
  			</div>
  			<div class="fones">
  				<div class="fone">
  					<?= $fone1 ?>
  				</div>
  				<div class="fone">
  					<?= $fone2 ?>
  				</div>

  			</div>
  		</form>
  	</div>
  </section>
<?php endif; ?>
<!-- <?php include 'includes/information.php' ?>
<section id="reserva">
	<div class="container">
		<form action="https://hotels.cloudbeds.com/reservas/Cln8Hw" method="post">
			<div class="check in">
				<label for="checkin">Entrada</label>
					<input type="text" data-day="entrada" data-type="calendario" name="checkin" id="checkinFooter" value="">
					<input type="hidden" name="date_format" value="d/m/Y">
					<input type="hidden" name="widget_date" value="">
			</div>
			<div class="check out">
				<label for="checkout">Saída</label>
					<input type="text" data-day="saida" data-type="calendario" name="checkout" id="checkoutFooter" value="">
					<input type="hidden" name="widget_date_to" value="">
			</div>
			<div class="reserva">
				<button type="submit">Fazer Reserva</button>
			</div>
			<div class="fones">
				<div class="fone">
					<?= $fone1 ?>
				</div>
				<div class="fone">
					<?= $fone2 ?>
				</div>

			</div>
		</form>
	</div>
</section> -->

<footer>
	<div class="container">
		<div class="seguro">
			<img src="<?php bloginfo('template_url') ?>/img/seguranca.png" alt="" />
		</div>
		<div class="pagamentos">
			<p>
				Métodos de pagamento:
			</p>
			<img src="<?php bloginfo('template_url') ?>/img/metodos.png" alt="" />
		</div>
	</div>
</footer>
<?php include 'includes/preheader.php' ?>

<section class="footer-subs">
	<div class="container">
		<h1>Receba Novidades e Promoções por email!</h1>
		<form action="//pousadacamelot.us13.list-manage.com/subscribe/post?u=cbf8d3c00193be30342979a66&amp;id=d6d3f275fe" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
			<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
			<button type="submit">Cadastrar E-mail</button>
		</form>

		<div class="divisao">

		</div>

		<ul>
			<li><?= $email_forms ?></li>
			<li><?= $fone1 ?> | <?= $fone2 ?></li>
		</ul>

		<div class="divisao">

		</div>
		<div class="center">
			<ul class="social">
				<li><a href="https://www.facebook.com/camelotpousada" target="_blank" class="facebook"></a></li>
				<li><a href="https://www.instagram.com/pousadacamelot/" target="_blank" class="instagram"></a></li>
			</ul>
		</div>
	</div>
</section>

<div class="modal faleConosco">
	<div class="container">
		<div class="close-pi">
			x
		</div>

		<div class="text">

			<h2>Fale Conosco</h2>
			<div class="divisao">

			</div>
			<p>
				Podemos ajudar de alguma forma? Entre em contato por telefone, por email ou pelo formulário de contato abaixo. Retornamos dentro de 24h.
			</p>
			<ul>
				<li>• <?=$email_forms?></li>
				<li>• <?= $fone1 ?> | <?= $fone2 ?></li>
			</ul>

		</div>

		<div class="form">
			<form id="formFale">
				<div class="fields">
					<div class="field">
						<input type="text" name="name" class="required" placeholder="nome*">
					</div>
					<div class="field">
						<input type="text" name="phone" class="required" placeholder="telefone*">
					</div>
					<div class="field">
						<input type="email" name="email" placeholder="email">
					</div>
				</div>
				<div class="textarea">
					<textarea name="message" class="required" placeholder="mensagem*"></textarea>
					<button type="submit">ENVIAR</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal trabalheConosco">
	<div class="container">
		<div class="close-pi">
			x
		</div>

		<div class="text">

			<h2>Trabalhe Conosco</h2>
			<div class="divisao">

			</div>
			<p>Quer fazer parte da nossa equipe, aproveitar as oportunidades e crescer junto com a gente? Estamos sempre em busca de pessoas motivadas e dispostas a colocar a mão na massa! Afinal, é com bastante empenho e disposição que a gente consegue oferecer um ótimo serviço para os nossos hóspedes e clientes!</p>
			<p>Informe os seus dados abaixo, anexe seu currículo e entraremos em contato!</p>

		</div>

		<div class="form">
			<form class="" id="formTrabalhe">
				<div class="fields">
					<div class="field">
						<input type="text" name="name" class="required" placeholder="nome*">
					</div>
					<div class="field">
						<input type="text" name="phone" class="required" placeholder="telefone*">
					</div>
					<div class="field">
						<input type="email" name="email" placeholder="email">
					</div>
				</div>
				<div class="textarea">
					<textarea name="message" class="required" placeholder="mensagem*"></textarea>

				</div>
        <div class="fields fieldsCV">
          <div class="field">

            <button type="submit">ENVIAR</button>
            <label for="cv" class="labelFile">Anexar currículo</label>
            <input type="file" name="cv" id="cvFile">

          </div>


        </div>
			</form>
		</div>
	</div>
</div>

<div class="modal orcamento">
	<div class="container">
		<div class="close-pi">
			x
		</div>

		<div class="text">

			<h2>Peça um Orçamento</h2>
			<div class="divisao">

			</div>
			<p>Nos passe as suas informações de contato e especifique quais são as características do evento que você pretende fazer em Camelot.</p>
			<p>Assim, poderemos te retornar para apresentar um orçamento.</p>

		</div>

		<div class="form">
			<form class="" id="formOrcamento">
				<div class="fields">
					<div class="field">
						<input type="text" name="name" class="required" placeholder="nome*">
					</div>
					<div class="field">
						<input type="text" name="phone" class="required" placeholder="telefone*">
					</div>
					<div class="field">
						<input type="email" name="email" placeholder="email">
					</div>
				</div>
				<div class="textarea">
					<textarea name="message" class="required" placeholder="mensagem*"></textarea>
					<button type="submit">ENVIAR</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal contratarPacotePasseio">
	<div class="container">
		<div class="close-pi">
			x
		</div>

		<div id="contratar" class="contratar">
			<div class="info"> </div>
				<div class="text">
					<h4>QUER CONTRATAR ALGUM PACOTE OU PASSEIO?</h4>
					<p>Os pacotes e os passeios serão oferecidos no momento da reserva. Então, escolha o seu pacote ou passeio e clique já em "Fazer Reserva"!</p>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal contratarPacoteSpa">
	<div class="container">
		<div class="close-pi">
			x
		</div>

		<div id="contratar" class="contratar">
			<div class="info"> </div>
				<div class="text">
					<h4>QUER CONTRATAR ALGUM PACOTE OU TERAPIA?</h4>
					<p>Os pacotes e as terapias serão oferecidos no momento da reserva. Então, escolha o seu pacote ou terapia e clique já em "Fazer Reserva"!</p>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal lightbox">
	<div class="container">
		<div class="close-pi">
			x
		</div>
		<div class="img">
			<img src="" alt="" />
		</div>
	</div>
</div>

<div id="dpFull" data-role="popup" data-dismissible="false" data-history="false" data-position-to="window">
	<div class="container">
		<div class="close-pi">
			x
		</div>

	</div>
</div>

<div class="modal videobox">
	<div class="container" style="overflow: hidden">
		<div class="close-pi">
			x
		</div>
		<div class="img">
			<iframe width="90%" style="height: 70vh; margin: 0 auto; display: block" src="https://www.youtube.com/embed/q6jCjo55eu4?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
		</div>
	</div>
</div>

<script src="<?php bloginfo('template_url') ?>/js/jquery.js"></script>
<script type="text/javascript">

function initMap() {

  var camelot = {lat: -14.130552, lng: -47.522969};

  var map;
  var iconBaseBlue = "<?php bloginfo('template_url')?>/img/mapa_pin.png?=123123";

  var styleArray = [{"stylers":[{"hue":"#ff1a00"},{"invert_lightness":true},{"saturation":-100},{"lightness":33},{"gamma":0.5}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#2D333C"}]}];

  map = new google.maps.Map(document.getElementById('map'), {
    center: camelot,
    zoom: 16,
    styles: styleArray,
    draggable: false,
    scrollwheel: false

  });
  // Create a marker and set its position.
  var marker = new google.maps.Marker({
    map: map,
    position: camelot,
    icon: iconBaseBlue
  });


  function moveToLocation(lat, lng){
    var center = new google.maps.LatLng(lat, lng);
    // using global variable:
    map.panTo(center);
    map.setZoom(14);
  }

  $('section#unidades ul li').click(function(){

    eval('var place = ' + $(this).attr('data-id') + ';');
    moveToLocation(place.lat, place.lng);
    $('section#unidades p').html($(this).attr('data-endereco'));

  });
}




		</script>
<script async defer
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDxvkEzFl7J0DNlvnyVmMW23Byx07KmN9I&callback=initMap">
</script>
<script src="<?php bloginfo('template_url') ?>/js/validate.js"></script>
<script>
  var siteURL = "<?php bloginfo('url') ?>";
</script>
<script src="https://hammerjs.github.io/dist/hammer.js"></script>
<script src="<?php bloginfo('template_url') ?>/js/jquery-ui.js"></script>
<script src="<?php bloginfo('template_url') ?>/js/app.js?=1"></script>
<script src="https://www.jscache.com/wejs?wtype=certificateOfExcellence&amp;uniq=441&amp;locationId=2348776&amp;lang=pt&amp;year=2016&amp;display_version=2"></script>


<!--Google Analytics-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-73679857-1', 'auto');
  ga('send', 'pageview');

</script>


<?php wp_footer(); ?>
</body>
</html>

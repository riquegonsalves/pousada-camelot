<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>


		<?php

		while ( have_posts() ) : the_post(); ?>
    <?php
      $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
    ?>

		<?php if(get_field('slider')): $contador = 0;?>

				<?php while(has_sub_field('slider')): ?>
					<?php if($contador==0){
						  $feat_image =  get_sub_field('imagem');
					} ?>
				<?php $contador++; endwhile; ?>

		<?php endif; ?>

    <section class="banner" id="bannerHM" style="background: url('<?= $feat_image ?>')">

      <div class="slider">
        <div class="container">

          <div class="content content-1">
            <div class="text">
              <h1><?php the_title() ?></h1>
            </div>



            <div class="reserva">
              <form action="/checkout" method="get">
                <h1>FAÇA SUA <span>RESERVA ONLINE</span></h1>
                <div class="check check-in"><span>Entrada:</span><input name="date_format" type="hidden" value="d/m/Y" /> <input name="widget_date" type="hidden" value="" /> <input readonly id="checkinBanner" name="checkin" type="text" value="" data-day="entrada" data-type="calendario" /></div>
                <div class="check check-out"><span>Saída:</span><input name="widget_date_to" type="hidden" value="" /> <input readonly id="checkoutBanner" name="checkout" type="text" value="" data-day="saida" data-type="calendario" /></div>
                <button type="submit">Fazer reserva</button> <span class="ou">OU LIGUE</span>
                <?php include 'includes/telefones.php' ?>
              </form>
            </div>
          </div>

					<?php if(get_field('slider')): $contador = 0;?>

						<div class="navigation">
							<?php while(has_sub_field('slider')): ?>
								<div data-bg="<?php the_sub_field('imagem') ?>" data-order="<?= $contador+1 ?>" class="option option-<?= $contador+1 ?> <?php if($contador == 0): ?>active<?php endif; ?>"></div>
							<?php $contador++; endwhile; ?>
						</div>
					<?php endif; ?>


        </div>
      </div>
    </section>

    <section class="why">
      <div class="container">
        <h1>Descrição</h1>
        <p>
          <?php the_content() ?>
        </p>
      </div>
    </section>

    <section class="infos">
      <div class="container">

        <h1>Comodidades</h1>
        <div class="lists">
          <ul>
            <li>QUARTO</li>
            <?php if(get_field('quarto')): ?>
              <?php while(has_sub_field('quarto')): ?>
                <li><div class="i"><img src="<?php the_sub_field('icone') ?>" alt="" /></div> <?php the_sub_field('item') ?></li>
              <?php endwhile; ?>
            <?php endif; ?>
          </ul>

          <ul>
            <li>ALIMENTAÇÃO</li>
            <?php if(get_field('alimentacao')): ?>
              <?php while(has_sub_field('alimentacao')): ?>
                <li><div class="i"><img src="<?php the_sub_field('icone') ?>" alt="" /></div> <?php the_sub_field('item') ?></li>
              <?php endwhile; ?>
            <?php endif; ?>
          </ul>
        </div>

        <div class="line">

        </div>

        <h1>Preços</h1>

        <ul class="prices">

					<?php if(get_field('price_two')): ?>
	          <li>
	            <div class="left">
	              <div class="persons two"></div>
	              <div class="text">
	                (até duas pessoas)
	              </div>
	            </div>
	            <div class="value">a partir de <span>R$ <?= get_field('price_two')?></span></div>
	          </li>
					<?php endif; ?>
					<?php if(get_field('price_three')): ?>
	          <li>
	            <div class="left">
	              <div class="persons three"></div>
	              <div class="text">
	                (três pessoas)
	              </div>
	            </div>
	            <div class="value">a partir de <span>R$ <?= get_field('price_three')?></span></div>
	          </li>
					<?php endif; ?>
					<?php if(get_field('price_four')): ?>
	          <li>
	            <div class="left">
	              <div class="persons four"></div>
	              <div class="text">
	                (quatro pessoas)
	              </div>
	            </div>
	            <div class="value">a partir de <span>R$ <?= get_field('price_four')?></span></div>
	          </li>
					<?php endif; ?>
        </ul>
        <div class="obs">
          * para consultar os valores de baixa/alta temporada, <a href="/tarifario">veja nosso tarifário.</a>
        </div>

      </div>
    </section>


		<?php if(get_field('more')): ?>
			<section class="more">
	      <div class="container">
	        <h1>Mais informações</h1>

	        <ul>
						<?php while(has_sub_field('more')): ?>
	          	<li>• <?php the_sub_field('informacao') ?></li>
	        	<?php endwhile; ?>
	        </ul>

	      </div>
	    </section>

			<?php endif; ?>




    <?php
		  endwhile;	?>

<?php get_footer(); ?>

<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>


		<?php

		while ( have_posts() ) : the_post(); ?>
    <?php
      $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
    ?>

    <section class="banner" style="background: url('<?= $feat_image ?>')">

      <div class="slider">
        <div class="container">

          <div class="content content-1">
            <div class="text">
              <h1><?php the_title() ?></h1>
            </div>
						<?php
							$entrada = get_field('entrada');
							$entrada = new DateTime($entrada);

              //variável criada por conta do bug que tira 1 dia da data original
              $entradabug = get_field('entrada');
              $entradabug = new DateTime($entradabug);
							$entradabug->modify('+1 day');

							$saida = get_field('saida');
							$saida = new DateTime($saida);

              //variável criada por conta do bug que tira 1 dia da data original
              $saidabug = get_field('saida');
              $saidabug = new DateTime($saidabug);
							$saidabug->modify('+1 day');
						 ?>
						 <div class="preCheckin" data-date="<?= $entradabug->format('Y-m-d') ?>"></div>
						 <div class="preCheckout" data-date="<?= $saidabug->format('Y-m-d') ?>"></div>
            <div class="reserva">
              <form action="/checkout" method="get">
                <h1>FAÇA SUA <span>RESERVA ONLINE</span></h1>
                <div class="check check-in"><span>Entrada:</span><input name="date_format" type="hidden" value="d/m/Y" /> <input name="widget_date" type="hidden" value="" /> <input readonly id="checkinBanner" name="checkin" type="text" value="" disabled="disabled" data-day="entrada" data-type="calendario" /></div>
                <div class="check check-out"><span>Saída:</span><input name="widget_date_to" type="hidden" value="" /> <input readonly id="checkoutBanner" disabled="disabled" name="checkout" type="text" value="" data-day="saida" data-type="calendario" /></div>
                <button type="submit">Fazer reserva</button> <span class="ou">OU LIGUE</span>
              	<?php include 'includes/telefones.php' ?>
              </form>
							<div class="alteracoes">
								* Para alterações no pacote, ligue <?=$fone1?>
	 						</div>
            </div>

          </div>

        </div>
      </div>
    </section>

    <section class="why">
      <div class="container">
        <h1>Descrição</h1>
        <p>
          <?php the_content() ?>
        </p>
      </div>
    </section>

    <section class="infos">
      <div class="container">

        <h1>Itens Inclusos</h1>
        <div class="lists">
          <ul>
            <li>PACOTE</li>
            <?php if(get_field('itens')): ?>
              <?php while(has_sub_field('itens')): ?>
                <li><div class="i"><img src="<?php the_sub_field('icone') ?>" alt="" /></div> <?php the_sub_field('item') ?></li>
              <?php endwhile; ?>
            <?php endif; ?>
          </ul>
        </div>

        <div class="line">

        </div>

        <h1>Preços</h1>

        <ul class="prices">
          <li>
            <div class="left">
              <div class="persons two"></div>
              <div class="text">
                (até duas pessoas)
              </div>
            </div>
            <div class="value">a partir de <span>R$ <?= get_field('price_two')?></span></div>
          </li>
          <li>
            <div class="left">
              <div class="persons three"></div>
              <div class="text">
                (três pessoas)
              </div>
            </div>
            <div class="value">a partir de <span>R$ <?= get_field('price_three')?></span></div>
          </li>
          <li>
            <div class="left">
              <div class="persons four"></div>
              <div class="text">
                (quatro pessoas)
              </div>
            </div>
            <div class="value">a partir de <span>R$ <?= get_field('price_four')?></span></div>
          </li>
        </ul>
        <div class="obs">
          * Estes valores são referentes à <a href="<?php bloginfo('url') ?>/acomodacao/suite-luxo/">Suíte Luxo.</a> Para consultar valores de outras acomodações <a href="/checkout?link=true&widget_date=<?= $entrada->format('d-m-Y') ?>&widget_date_to=<?= $saida->format('d-m-Y') ?>">clique aqui.</a>
        </div>

      </div>
    </section>

    <?php if(get_field('more')): ?>
			<section class="more">
	      <div class="container">
	        <h1>Mais informações</h1>

	        <ul>
						<?php while(has_sub_field('more')): ?>
	          	<li>• <?php the_sub_field('informacao') ?></li>
	        	<?php endwhile; ?>
	        </ul>

	      </div>
	    </section>

			<?php endif; ?>


    <?php
		  endwhile;	?>

<?php get_footer(); ?>

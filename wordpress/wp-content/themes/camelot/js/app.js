$(document).mouseup(function (e)
{
    var container = $(".modal .container");

    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
    {

  		if($('.modal.videobox').css('left') == 0){
          	$(".videobox iframe")[0].src = $(".videobox iframe")[0].src.replace("&autoplay=1", "&autoplay=0");
  		}
      if($('.datepickerModal').css('left') == '0px'){
    		$('.modal').css('left', '-100vw');
        $('.datepickerModal').css('left', '0px');
      }else{
        $('.modal').css('left', '-100vw');
      }

    }

    if(!$("input").datepicker( "widget" ).is(":visible")){
    	$('.datepickerModal').css('left', '-100vw');
    }

});

$(function() {
  $('input[readonly]').on('focus', function(ev) {
    $(this).trigger('blur');
  });
});

function onLoadHandler(){
	$('#carousel-example-generic').css('width', '100%');
}

$(document).ready(function(){
	

  $.extend($.datepicker,{_checkOffset:function(inst,offset,isFixed){return offset}});

    $("#checkinBanner, #checkoutBanner, #checkinFooter, #checkoutFooter, #checkinHeader, #checkoutHeader, #checkinH, #checkoutH").datepicker({
        dateFormat: 'DD, d M',
        dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'],
        // dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
        dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
        dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
        monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
        monthNamesShort: ['JAN','FEV','MAR','ABR','MAI','JUN','JUL','AGO','SET','OUT','NOV','DEZ'],
        beforeShow: function(input, inst) {


            if(!$('.datepickerModal').length){

              $('#ui-datepicker-div').wrap(function() {
                return "<div class='modal datepickerModal' style='left: 0px'></div>";
              });


              // $('.ui-datepicker-div').click(function(event){
              //     event.stopPropagation();
              //     console.log("parou a propagação");
              // });


              // $('.datepickerModal').click(function(){
              //   $('.datepickerModal').css('left', '-100vw');
              //   $(input).datepicker("hide");
              // });

            }

            setTimeout(function(){
              $('.datepickerModal').css('left', '0px');
            }, 0);


        },
		onSelect: function(d){

			console.log(d);

			$('.datepickerModal').css('left', '-100vw');

			 var date= $(this).datepicker('getDate');
			 var day = date.getDate();
			 var monthIndex = date.getMonth()+1;
			 var year = date.getFullYear();

			 if($(this).attr('name') == 'checkin'){
				 $(this).siblings('[name="widget_date"]').val(day+'/'+monthIndex+'/'+year);
		         var tomorrow = new Date(date);
		         tomorrow.setDate(tomorrow.getDate() + 1);
		         $('input[name="checkin"]').datepicker('setDate', date);
		         $('input[name="checkout"]').datepicker('setDate', tomorrow);
			 }

			 if($(this).attr('name') == 'checkout'){
				 $(this).siblings('[name="widget_date_to"]').val(day+'/'+monthIndex+'/'+year);
		         $('input[name="checkout"]').datepicker('setDate', date);
			 }

			 $(this).val(d);

		},

		onClose: function(){
			$('.datepickerModal').css('left', '-100vw');
		}

    });




		if($('.preCheckin').length){
			var entrada = $('.preCheckin').attr('data-date');
			entrada = new Date(entrada);
			var saida = $('.preCheckout').attr('data-date');
			saida = new Date(saida);

			var day = entrada.getDate();
		 	var monthIndex = entrada.getMonth()+1;
	 		var year = entrada.getFullYear();
			$('input[name="widget_date"]').val(day+'/'+monthIndex+'/'+year)
			day = saida.getDate();
			monthIndex = saida.getMonth()+1;
			year = saida.getFullYear();
			$('input[name="widget_date_to"]').val(day+'/'+monthIndex+'/'+year)


			$("input[data-day='entrada']").attr('disabled', 'disabled').addClass('disabled');
			$("input[data-day='saida']").attr('disabled', 'disabled').addClass('disabled');

			$("input[data-day='entrada']").datepicker('setDate', entrada);
			$("input[data-day='saida']").datepicker('setDate', saida);
		} else {

			$("input[data-day='entrada']").datepicker('setDate', new Date());
			var today = new Date();
			var day = today.getDate();
			var monthIndex = today.getMonth()+1;
			var year = today.getFullYear();
			$('input[name="widget_date"]').val(day+'/'+monthIndex+'/'+year);
			var tomorrow = new Date();
			tomorrow.setDate(tomorrow.getDate() + 1);

			$("input[data-day='saida']").datepicker('setDate', tomorrow);
			day = tomorrow.getDate();
			monthIndex = tomorrow.getMonth()+1;
			year = tomorrow.getFullYear();
			$('input[name="widget_date_to"]').val(day+'/'+monthIndex+'/'+year);
		}
});

$(document).ready(function(){
	$('#formFale').validate({
		submitHandler: function(form){

			var formulario = $('#formFale');
			var nome = $('#formFale input[name="name"]').val();
			var email = $('#formFale input[name="email"]').val();
			var fone = $('#formFale input[name="phone"]').val();
			var mensagem = $('#formFale textarea[name="message"]').val();



			$.ajax({
				type: "POST",
				url: siteURL+"/wp-admin/admin-ajax.php",
				data: {
					action: 'contatoAction',
					nome:nome,
					email:email,
					mensagem:mensagem,
					tipo: 'Fale Conosco'
				},
				dataType: 'JSON',
				success:function(response){
					alert('Formulário enviado com sucesso!');
					$('#formFale button').html('Enviado com sucesso!');
					$('#formFale input,  #formFale textarea').val('');

				},
				error:function(response){
					$('#formFale button').html('Erro ao enviar! Tente novamente');
					$('#formFale input,  #formFale textarea').val('');
				}
			});
			return false;
		}
	});

	$('#formTrabalhe').validate({
		submitHandler: function(form){

			var formulario = $('#formTrabalhe');
			var nome = $('#formTrabalhe input[name="name"]').val();
			var email = $('#formTrabalhe input[name="email"]').val();
			var fone = $('#formTrabalhe input[name="phone"]').val();
			var mensagem = $('#formTrabalhe textarea[name="message"]').val();
			var cv = $('#formTrabalhe input[name="cv"]').prop('files');

	    var formData = new FormData();
	    formData.append('nome', nome);
	    formData.append('action', 'contatoAction');
	    formData.append('email', email);
	    formData.append('fone', fone);
	    formData.append('mensagem', mensagem);
	    formData.append('tipo', 'Trabalhe Conosco');
	    formData.append('cv', $('#formTrabalhe input[name="cv"]')[0].files[0]);

	    for (var pair of formData.entries()) {
	      console.log(pair[0]+ ', ' + pair[1]);
	    }


			$.ajax({
				type: "POST",
				url: siteURL+"/wp-admin/admin-ajax.php",
				data: formData,
	      contentType: false,
	      processData: false,
				success:function(response){
					alert('Formulário enviado com sucesso!');
					$('#formTrabalhe button').html('Enviado com sucesso!');
					$('#formTrabalhe input,  #formFale textarea').val('');

				}
			});
			return false;
		}
	});

	$('#formOrcamento').validate({
		submitHandler: function(form){

			var formulario = $('#formOrcamento');
			var nome = $('#formOrcamento input[name="name"]').val();
			var email = $('#formOrcamento input[name="email"]').val();
			var fone = $('#formOrcamento input[name="phone"]').val();
			var mensagem = $('#formOrcamento textarea[name="message"]').val();



			$.ajax({
				type: "POST",
				url: siteURL+"/wp-admin/admin-ajax.php",
				data: {
					action: 'contatoAction',
					nome:nome,
					email: email,
					mensagem:mensagem,
					tipo: 'Orçamento'
				},
				dataType: 'JSON',
				success:function(response){
					alert('Pedido de Orçamento enviado com sucesso!');
					$('#formOrcamento button').html('Enviado com sucesso!');
					$('#formOrcamento input,  #formFale textarea').val('');

				}
			});
			return false;
		}
	});
});



if($('.checkoutPage').length){
	function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
	}

	var widget_date = getParameterByName('widget_date'); // "lorem"
	var widget_date_to = getParameterByName('widget_date_to'); // "lorem"
  var get = getParameterByName('link');
	var date_format = getParameterByName('date_format'); // "lorem"


	widget_date = widget_date.replace("/", "-");
	widget_date = widget_date.replace("/", "-");
	widget_date_to = widget_date_to.replace("/", "-");
	widget_date_to = widget_date_to.replace("/", "-");

	var checkin = widget_date.split('-');
	var checkout = widget_date_to.split('-');
	var zero = '0';
  if(!get){
    if(checkin[1]<= 9){
      checkin[1] = zero.concat(checkin[1]);
    }
    if(checkout[1]<= 9){
      checkout[1] = zero.concat(checkout[1]);
    }
  }

	widget_date = checkin[2] + '-' + checkin[1] + '-' + checkin[0];
	widget_date_to = checkout[2] + '-' + checkout[1] + '-' + checkout[0];


	var userAgent = navigator.userAgent || navigator.vendor || window.opera;


    // iOS detection from: http://stackoverflow.com/a/9039885/177710
    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
        window.location.replace('https://hotels.cloudbeds.com/reservas/Cln8Hw#checkin='+widget_date+'&checkout='+widget_date_to);
    }else{
		$('.checkoutPage #iframeID').attr('src', 'https://hotels.cloudbeds.com/reservas/Cln8Hw#checkin='+widget_date+'&checkout='+widget_date_to).attr('width', '100%').attr('height', '2500px');
    }


}

$('.play').click(function(){
  $('.videobox').css('left', 0);

    if($(".videobox iframe")[0].src.search('autoplay') != -1){
      $(".videobox iframe")[0].src = $(".videobox iframe")[0].src.replace("&autoplay=0", "&autoplay=1");
    }else{
      $(".videobox iframe")[0].src += "&autoplay=1";
    }

});

// MENU ABRIR MOBILE
if($('.slider .navigation').length){
	var bg;
  var order;
	$('.slider .navigation .option').click(function(){
		$('.slider .navigation .option').removeClass('active');
		$(this).addClass('active');
		bg =  $(this).attr('data-bg');
		order =  $(this).attr('data-order');
		$('.banner').css('background-image', 'url("'+bg+'")').attr('data-order', order);
	});
}


//EVENTOS DE CLIQUE


$('#tour_cafe').click(function(){
	$('#tour_virtual').attr('src', siteURL+'/wp-content/themes/camelot/tour_virtual/amb2.swf');
	// event.preventDefault();
});
$('#tour_piscina').click(function(){
	$('#tour_virtual').attr('src', siteURL+'/wp-content/themes/camelot/tour_virtual/amb3.swf');
	// event.preventDefault();
});
$('#tour_suite_luxo').click(function(){
	$('#tour_virtual').attr('src', siteURL+'/wp-content/themes/camelot/tour_virtual/amb9.swf');
	// event.preventDefault();
});
$('#tour_suite_luxo_especial').click(function(){
	$('#tour_virtual').attr('src', siteURL+'/wp-content/themes/camelot/tour_virtual/amb9.swf');
	// event.preventDefault();
});
$('#tour_suite_superior').click(function(){
	$('#tour_virtual').attr('src', siteURL+'m.br/wp-content/themes/camelot/tour_virtual/amb7.swf');
	// event.preventDefault();
});
$('#tour_suite_master').click(function(){
	$('#tour_virtual').attr('src', siteURL+'/wp-content/themes/camelot/tour_virtual/amb6.swf');
	// event.preventDefault();
});
$('#tour_area_externa').click(function(){
	$('#tour_virtual').attr('src', siteURL+'/wp-content/themes/camelot/tour_virtual/amb5.swf');
	// event.preventDefault();
});
$('.clickContratarPasseios').click(function(){
	$('.contratarPacotePasseio').css('left', 0);
	$('header').removeClass('aberto');
	event.preventDefault();
});
$('.clickContratarSpa').click(function(){
	$('.contratarPacoteSpa').css('left', 0);
	$('header').removeClass('aberto');
	event.preventDefault();
});
$('#menuLinks.pictures ul li a').click(function(){
	var li = $(this).parent('li');
	var img = li.attr('data-img');

	$('.modal.lightbox').css('left', 0);
	$('.modal.lightbox img').attr('src', img);

	event.preventDefault();
});
$('.l-fale').click(function(){
	$('.faleConosco').css('left', 0);
	$('header').removeClass('aberto');
	event.preventDefault();
});
$('.l-orcamento').click(function(){
	$('.modal.orcamento').css('left', 0);
	$('header').removeClass('aberto');
	event.preventDefault();
});
$('.l-trabalhe').click(function(){
	$('.trabalheConosco').css('left', 0);
	$('header').removeClass('aberto');
	event.preventDefault();
});

$('.modal .close-pi').click(function(){
	$('.modal').css('left', '-100vw');
  $(".videobox iframe")[0].src = $(".videobox iframe")[0].src.replace("&autoplay=1", "&autoplay=0");
	event.preventDefault();
});



$('.menu-link, .mobile-close').click(function(){
	$('header').toggleClass('aberto');
});

var mq = window.matchMedia( "(min-width: 860px)" );
var mq2 = window.matchMedia( "(max-width: 1125px)" );
var mq3 = window.matchMedia( "(max-width: 860px)" );

if(mq.matches){
	$(document).scroll(function(e){
		var scroll = $(document).scrollTop();

		if(scroll > 582){
			$('.fixheader').css('top', '0px');
		} else {
			$('.fixheader').css('top', '-100vh');
		}
	});
}

if(mq2.matches){
	$(document).scroll(function(e){
		var scroll = $(document).scrollTop();

		if(scroll > 400){
			$('.mobile-reserva button, .mobile-head').slideDown(300);
		} else {
			if(mq3.matches){
				$('.mobile-reserva button, .mobile-head.home-head').slideUp(300);
			}
		}
	});
}

$('.mobile-reserva button').click(function(){
	$(this).toggleClass('dois');
	$('#reserva.mobileFixed').slideToggle();
});


// Google Analytics

$('.reserva button').click(function(){
	ga('send', 'event', 'call to action', 'iniciar reserva');
});

$('btn green btn-lg btn-block book_now').click(function(){
	ga('send', 'event', 'call to action', 'confirmar reserva');
});

$(function() {
  $(".labelFile").click(function() {
    $("#cvFile").trigger('click');
  });

  $('#cvFile').change(function(e){
    $('.labelFile').html($('#cvFile').val());
  });
});

//função para fazer o readonly funcionar no ios
// $(".readonly").on("touchstart", function(e) {
//     e.preventDefault();
// }

$(function(){
  $('.navIcon').click(function(){
    if($('section.banner').attr('data-order')){
      var order = $('section.banner').attr('data-order');
    } else {
      var order = 1;
    }

    var bg;
    if($(this).hasClass('prevSlider')){
      if(order==1){
        bg = $('.slider .navigation .option:last-of-type').attr('data-bg');
        order = $('.slider .navigation .option:last-of-type').attr('data-order');
      } else {
        order--;
        bg = $('.slider .navigation .option[data-order='+order+']').attr('data-bg');
      }


    } else {
      order++;
      if(!$('.slider .navigation .option[data-order='+order+']').length){
        order = 1;
      }
      bg = $('.slider .navigation .option[data-order='+order+']').attr('data-bg');
    }

    $('.banner').css('background-image', 'url("'+bg+'")').attr('data-order', order);
    $('.slider .navigation .option').removeClass('active');
    $('.slider .navigation .option[data-order='+order+']').addClass('active');
  });

});

// função base que muda o banner para a direita
function changeImg(){
	var bg;

	//caso a variável order ainda não esteja definida, já dou o valor de 1. Fiz isso para resolver o bug do safari
	if(typeof order == "undefined"){
		order = 1;
	}

	order++;
	//no caso do slider acabar, volta pra um
	if(!$('.slider .navigation .option[data-order='+order+']').length){
	  order = 1;
	}
	bg = $('.slider .navigation .option[data-order='+order+']').attr('data-bg');

	$('.banner').css('background-image', 'url("'+bg+'")').attr('data-order', order);
	$('.slider .navigation .option').removeClass('active');
	$('.slider .navigation .option[data-order='+order+']').addClass('active');

	setTimeout("changeImg()", 5000);

}

// função para mudar o banner periodicamente se houver banner na página
if($('#bannerHM').length){
	setTimeout("changeImg()", 5000);
}

if($('#bannerHM').length){

	var myElement = document.getElementById('bannerHM');

	// create a simple instance
	// by default, it only adds horizontal recognizers
	var mc = new Hammer(myElement);

	// listen to events...
	mc.on("swipeleft swiperight", function(ev) {


	  if($('section.banner').attr('data-order')){
	    var order = $('section.banner').attr('data-order');
	  } else {
	    var order = 1;
	  }

	  var bg;
	  if(ev.type == 'swipeleft'){
	    order++;
	    if(!$('.slider .navigation .option[data-order='+order+']').length){
	      order = 1;
	    }
	    bg = $('.slider .navigation .option[data-order='+order+']').attr('data-bg');
	  }

	  if(ev.type == 'swiperight'){
	    if(order==1){
	      bg = $('.slider .navigation .option:last-of-type').attr('data-bg');
	      order = $('.slider .navigation .option:last-of-type').attr('data-order');
	    } else {
	      order--;
	      bg = $('.slider .navigation .option[data-order='+order+']').attr('data-bg');
	    }
	  }

	  $('.banner').css('background-image', 'url("'+bg+'")').attr('data-order', order);
	  $('.slider .navigation .option').removeClass('active');
	  $('.slider .navigation .option[data-order='+order+']').addClass('active');
	});

}
